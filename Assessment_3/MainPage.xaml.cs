﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using MySql.Data.MySqlClient;
using MySQLDemo.Classes;



namespace Assessment_3
{
   
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
           names.ItemsSource = loadAllTheData();
           
        }

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            MySplitView.IsPaneOpen = !MySplitView.IsPaneOpen;
        }

        void selectedName(string name)
        {
            var command = $"Select * from tbl_people WHERE FNAME = '{name}' ";
            var b = MySQLCustom.ShowInList(command);

            userAcc.Text = b[0];
            Fname.Text = b[1];
            Lname.Text = b[2];
            DOB.Text = b[3];
            Str_number.Text = b[4];
            Str_name.Text = b[5];
            Postcode.Text = b[6];
            City.Text = b[7];
            Phone1.Text = b[8];
            Phone2.Text = b[9];
            Email.Text = b[10];
        }

        static List<string> loadAllTheData()
        {
            //Set the command and executes it and returns a list
            var command = $"Select FNAME from tbl_people";

            //Call the custom method
            var list = MySQLCustom.ShowInList(command);

            //Display the list (in this case a combo list)
            return list;
        }

        void clearAll()
        {
            Fname.Text = "";
            Lname.Text = "";
            DOB.Text = "";
            userAcc.Text = "";
            Str_number.Text = "";
            Str_name.Text = "";
            Postcode.Text = "";
            City.Text = "";
            Phone1.Text = "";
            Phone2.Text = "";
            Email.Text = "";
        }


        void clearAll_added()
        {
            fname.Text = "";
            lname.Text = "";
            dob.Text = "";
            userAccount.Text = "";
            str_number.Text = "";
            str_name.Text = "";
            postcode.Text = "";
            city.Text = "";
            phone1.Text = "";
            phone2.Text = "";
            email.Text = "";
        }

        private void names_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Checks if the object still exists
            if (names.SelectedItem != null)
            {
                selectedName(names.SelectedItem.ToString());
            }
        }


        // Database update

        private void updateDB_Click(object sender, RoutedEventArgs e)
        {
            //Update Information in the Database
            MySQLCustom.UpdateData(fname.Text, lname.Text, dob.Text, userAccount.Text, str_number.Text, str_name.Text, postcode.Text, city.Text, phone1.Text, phone2.Text, email.Text);
            names.ItemsSource = loadAllTheData();
            clearAll();
        }

        private void addToDB_Click(object sender, RoutedEventArgs e)
        {
            //Add Information to the Database
            MySQLCustom.AddData(fname.Text, lname.Text, dob.Text, str_number.Text, str_name.Text, postcode.Text, city.Text, phone1.Text, phone2.Text, email.Text);
            names.ItemsSource = loadAllTheData();
            clearAll_added();
        }
        private void removeFromDB_Click(object sender, RoutedEventArgs e)
        {
            //Remove Information from the Database
            MySQLCustom.DeleteDate(userAcc.Text);
            names.ItemsSource = loadAllTheData();
            clearAll();
        }

        private void dob_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void backbutton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }

        private void close_app_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Exit();
        }
    }
}
